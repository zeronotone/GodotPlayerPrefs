#include "PlayerPrefs.h"
#include "base64.h"
#include "xxhash.c"
#include "pugixml.hpp"
#ifdef _WIN32
#include <windows.h>
#elif __APPLE__ || __linux__
#include "Plist.hpp"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

struct passwd *pw = getpwuid(getuid());

const char *homedir = pw->pw_dir;

string homepath(homedir);

pugi::xml_document doc;

#endif

string PrefsPath = "";

string EncryptionKey = "e806f6";

bool ExtraEncryption = false;

PlayerPrefs* PlayerPrefs::singleton = NULL;

PlayerPrefs::PlayerPrefs() {

	singleton = this;

}

PlayerPrefs* PlayerPrefs::get_singleton() {

	return singleton;

}

string PlayerPrefs::EncryptKey(string key) {

	key = EncryptDecryptBytes(key, key.length(), EncryptionKey);

	key = base64_encode((const unsigned char *)(key.c_str()), key.length());

	return key;

}

string PlayerPrefs::GetFullKey(string key) {

#ifdef _WIN32

	HKEY    hKey = NULL;

	if (RegOpenKeyEx(HKEY_CURRENT_USER, PrefsPath.c_str(), 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS) {

		for (int i = 0, res = ERROR_SUCCESS; res == ERROR_SUCCESS; i++) {

			CHAR  szValue[MAX_PATH] = { 0 };

			DWORD nValueLength = MAX_PATH;

			DWORD nSize = 0;

			if ((res = RegEnumValue(hKey, i, szValue, &nValueLength, 0, NULL, NULL, &nSize)) == ERROR_SUCCESS) {

				if (string(szValue).find(key) != string::npos) {

				RegCloseKey(hKey);

				return szValue;

				}

			}

		}

        	RegCloseKey(hKey);

	}

#endif

	return "";

}


void PlayerPrefs::SetGameData(String Company, String Game, bool UseExtraEncryption) {

#ifdef _WIN32

	PrefsPath = "Software\\" + Company.utf8().get_data() + "\\" + Game.utf8().get_data() + "\\";

#elif __linux__

	PrefsPath = homepath + "/.config/unity3d/" + Company.utf8().get_data() + "/" + Game.utf8().get_data() + "/prefs";

#elif __APPLE__

	PrefsPath = homepath + "/Library/Preferences/unity." + Company.utf8().get_data() + "." + Game.utf8().get_data() + ".plist";

#endif

	ExtraEncryption = UseExtraEncryption;

}

bool PlayerPrefs::HasKey(String Name) {

	bool result = false;

	string key = EncryptKey(Name.utf8().get_data());

	if (key == "") {

		return result;

	}

#ifdef _WIN32

	key = GetFullKey(key);

	if (key == "") {

		return result;

	}

	HKEY    hKey = NULL;

	if (RegOpenKeyEx(HKEY_CURRENT_USER, PrefsPath.c_str(), 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS) {

		for (int i = 0, res = ERROR_SUCCESS; res == ERROR_SUCCESS; i++) {

			CHAR  szValue[MAX_PATH] = { 0 };

			DWORD nValueLength = MAX_PATH;

			DWORD nSize = 0;

			if ((res = RegEnumValue(hKey, i, szValue, &nValueLength, 0, NULL, NULL, &nSize)) == ERROR_SUCCESS) {

				if (szValue == key) {

					RegCloseKey(hKey);

					result = true;

				}

			}

		}

		RegCloseKey(hKey);

	}

#elif __linux__

	pugi::xml_parse_result load_prefs = doc.load_file(PrefsPath.c_str());

	if (load_prefs) {

		result = !doc.child("unity_prefs").find_child_by_attribute("pref", "name", key.c_str()).empty();

	}

#elif __APPLE__

	ifstream prefs_file(PrefsPath.c_str());

	if ((bool)prefs_file) {

		map<string, boost::any> dict;

		Plist::readPlist(PrefsPath.c_str(), dict);

		if (dict.count(key) != 0) {

			result = true;

		}

	}

#endif

	return result;

}

String PlayerPrefs::GetString(String Name) {

	string Data = GetData(Name, true);

	if (Data == "") {

		return "";

	}

	return Data.c_str();

}

int PlayerPrefs::GetInt(String Name) {

	const char* Data = GetData(Name, false);

	if ((string)Data == "") {

		return 0;

	}

	return *(int *)Data;

}

bool PlayerPrefs::GetBool(String Name) {

	const char* Data = GetData(Name, false);

	if ((string)Data == "") {

		return false;

	}

	return *(bool *)Data;

}

const char* PlayerPrefs::GetData(String Name, bool isString) {

	const char* result = "";

	string inputBytes;

	string encryptedInput = "";

	string keyName = Name.utf8().get_data();

	string key = EncryptKey(keyName);

	if (key == "") {

		return result;

	}

#ifdef _WIN32

	key = GetFullKey(key);

	if (key == "") {

		return result;

	}

	DWORD  bufSize   = 0;
	HKEY   hKey      = 0;

	char   buf[2048] = { 0 };

	bufSize = sizeof(buf);

	if (RegOpenKeyEx(HKEY_CURRENT_USER, PrefsPath.c_str(), 0, KEY_READ, &hKey) == ERROR_SUCCESS);
		if (RegQueryValueEx(hKey, key.c_str(), NULL, NULL, (LPBYTE)buf, &bufSize) == ERROR_SUCCESS)

			if (isString == true) {

				encryptedInput = string(buf);

			} else {

				encryptedInput = *(DWORD *)buf;

			}

#elif __linux__

	pugi::xml_parse_result load_prefs = doc.load_file(PrefsPath.c_str());

	if (load_prefs) {

		encryptedInput = doc.child("unity_prefs").find_child_by_attribute("pref", "name", key.c_str()).child_value();

	}

#elif __APPLE__

	ifstream prefs_file(PrefsPath.c_str());

	if ((bool)prefs_file) {

		map<string, boost::any> dict;

		Plist::readPlist(PrefsPath.c_str(), dict);

		encryptedInput = boost::any_cast<const int64_t&>(dict.find(key)->second);

	}

#endif

	if (ExtraEncryption == true) {

		try {

#ifdef __linux__
			inputBytes = base64_decode(base64_decode(encryptedInput));
#else
			inputBytes = base64_decode(encryptedInput);
#endif

		} catch (...) {

			return "";

		}

		if (inputBytes.length() <= 0) {

			return "";

		}

		int inputLength = inputBytes.length();

		unsigned char dataHashBytes[4] = { 0 };

		memcpy(dataHashBytes, inputBytes.c_str() + (inputLength - 4), 4);

		unsigned int inputDataHash = (unsigned int)(dataHashBytes[0] | dataHashBytes[1] << 8 | dataHashBytes[2] << 16 | dataHashBytes[3] << 24);

		int dataBytesLength = inputLength - 7;

		string encryptedBytes = "";

		for (int i = 0; i < dataBytesLength; i++) {

			encryptedBytes += inputBytes[i];

		}

		string cleanBytes = EncryptDecryptBytes(encryptedBytes, dataBytesLength, keyName + EncryptionKey);

		unsigned int realDataHash = XXH32(cleanBytes.c_str(), cleanBytes.length(), 0);

		if (realDataHash != inputDataHash) {

			return result;

		}

		result = cleanBytes.c_str();


	} else {

		try {

#ifdef __linux__
			result = base64_decode(encryptedInput).c_str();
#else
			result = encryptedInput.c_str();
#endif

		} catch (...) {

			return "";

		}

	}



	return result;

}

string PlayerPrefs::EncryptDecryptBytes(string bytes, int dataLength, string key) {

	int encryptionKeyLength = key.length();

	string result = "";

	for (int i = 0; i < dataLength; i++) {

		result += bytes[i] ^ key[i % encryptionKeyLength];

	}

	return result;

}

void PlayerPrefs::_bind_methods() {

	ObjectTypeDB::bind_method(_MD("SetGameData", "Company", "Game", "UseExtraEncryption"),&PlayerPrefs::SetGameData);

	ObjectTypeDB::bind_method(_MD("GetString", "Name"),&PlayerPrefs::GetString);

	ObjectTypeDB::bind_method(_MD("GetBool", "Name"),&PlayerPrefs::GetBool);

	ObjectTypeDB::bind_method(_MD("GetInt", "Name"),&PlayerPrefs::GetInt);

	ObjectTypeDB::bind_method(_MD("HasKey", "Name"),&PlayerPrefs::HasKey);

}

PlayerPrefs::~PlayerPrefs(){

	singleton = NULL;

}


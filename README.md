### Usage ###

```
PlayerPrefs.SetGameData(String Company, String Game, bool UseExtraEncryption)
```
Set game's data to recieve playerprefs. Should be declared before getting variables.

Note: UseExtraEncryption should be set "true" in case your Unity3D game uses ["Anti-Cheat Toolkit"](https://www.assetstore.unity3d.com/en/#!/content/10395) plugin to encrypt prefs. For default prefs it should be "false".

```
PlayerPrefs.GetString(String Name)
```
Get string from player prefs.

```
PlayerPrefs.GetBool(String Name)
```
Get bool from player prefs.

```
PlayerPrefs.GetInt(String Name)
```
Get int from player prefs.

```
PlayerPrefs.HasKey(String Name)
```
Check if key exists in player prefs.

### Licenses ###

* [Boost](https://github.com/blackberry/Boost): Boost Software License v1.0

* [PlistCpp](https://github.com/animetrics/PlistCpp): MIT license

* [pugixml](https://github.com/zeux/pugixml): MIT license

* [xxHash](https://github.com/Cyan4973/xxHash): BSD 2-Clause license

* [base64](http://www.adp-gmbh.ch/cpp/common/base64.html): zlib/libpng license


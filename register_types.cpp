#include "register_types.h"
#include "object_type_db.h"
#include "PlayerPrefs.h"
#include "globals.h"

static PlayerPrefs* PlayerPrefsPtr = NULL;

void register_GodotPlayerPrefs_types(){

	PlayerPrefsPtr = memnew(PlayerPrefs);

	Globals::get_singleton()->add_singleton(Globals::Singleton("PlayerPrefs",PlayerPrefs::get_singleton()));

	ObjectTypeDB::register_virtual_type<PlayerPrefs>();

}

void unregister_GodotPlayerPrefs_types(){

	memdelete(PlayerPrefsPtr);

}

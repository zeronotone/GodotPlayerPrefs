#ifndef PLAYER_PREFS
#define PLAYER_PREFS

#include "reference.h"

#include <string>

using namespace std;

class PlayerPrefs : public Reference {

protected:

	static string EncryptKey(string key);

	static string GetFullKey(string key);

	static void _bind_methods();

	static PlayerPrefs* singleton;

public:

	PlayerPrefs();

	~PlayerPrefs();

	static PlayerPrefs* get_singleton();

	void SetGameData(String Company, String Game, bool UseExtraEncryption);

	bool HasKey(String Name);

	String GetString(String Name);

	int GetInt(String Name);

	bool GetBool(String Name);

	const char* GetData(String Name, bool isString);

	static string EncryptDecryptBytes(string bytes, int dataLength, string key);

private:

	OBJ_TYPE(PlayerPrefs, Object);

	OBJ_CATEGORY("PlayerPrefs");

};

#endif
